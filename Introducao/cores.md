# Cores

## Cores por Nome

Os navegadores suportam um total de 140 nomes por nome [white, black, red, green, blue, etc]

## Cores por RGB

Existe uma função que pode ser passado três parâmetros **rgb(RED, GREEN, BLUE)**, cada um representando uma cor, de números inteiros de 0 a 255. Os nomes das funções devem ficar junto com o () [parenteses]

- vermelho => 0 a 255 => rgb(255, 0, 0)
- green => 0 a 255 => rgb(0, 255, 0)
- blue => 0 a 255 => rgb(0, 0, 255)

## Cores por Hexadecimal

Representa o *RGB* e é realizado colocando-se um # no inicio e seguido por 3 pares de letras. Os pares correspondem as cores vermelho, verde e azul, respectivamente **#RRGGBB**. Os valores de cada para vão de **00** a **FF** **[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F]**.

```css
.cor{
    background: #FF5566;
} 
```

## Cores por HSL

Hue (0 a 360), Saturation (0% a 100%), Lightness (0% a 100%); **Matiz, Saturação e Brilho**, respectivamente. É uma função que passamos três parâmetros **hsl(H, S, L)**.

## Cores e Transparência

Na função **rgba(RED, GREEN, BLUE, ALPHA)** e **hsla(H, S, L, A)** o alpha varia entre 0 e 1 (números reais). No hexadecimal **#RRGGBBAA** o alpha varia entre 00 e FF.
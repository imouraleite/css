# Seletores

## Seletor Universal = *

todas as tags vão aplicar as propriedades

``` css
* {
    background: black;
}
```

## Seletor de Tipo

aplica-se em todas as tags html especificada

``` css
body {
    font-size: 10em;
}
```

## Seletor por Classe

aplica-se a todas as classes de tags declaradas

``` css
.nome-da-classe {
    background: black;
}
```

## Seletor por Id

aplica-se a todos os Ids de tags html declarados

``` css
#nome-do-id {
    background: black;
}
```

## Seletor por Atributo

aplica-se a atributos de tags html que foram declarados

``` css
[type="text"] {
    background: black;
}
```

## Seletor por Pseudo-Seletor

são palavras reservadas que são semelhantes a ações no html (onMouseHover)

``` css
:hover {
    background: black;
} 
```

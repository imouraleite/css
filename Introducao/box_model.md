# Box Model

> Todos os elementos HTML podem ser considerados como caixas. Consistem em: **margens, bordas, preenchimento e conteudo**.

## Box Model - Campos

- Conteudo = É o que vai dentro da tag;
- Preenchimento (padding) = Espaço entre a borda e o conteudo;
- Borda (border) = Linhas de contorno do elemento;
- Margem (margin) = Espaçamento entre elementos/tags;

![alt](/Introducao/imagem/box_model_1.jpg)

## Box Model - Ordem

A ordem das dimensões do box model começam pelo top e seguem no sentido horário para as demais

![alt](/Introducao/imagem/box_model_2.jpg)

## Box Model - Abreviações

Quando inserimos apenas uma medida para um propriedade, esse valor vale para os quatros lados. Quando inserimos duas medidas, o valor é para top + bottom e right + left, respectivamente

![alt](/Introducao/imagem/box_model_3.jpg)

Para todos os lados temos:

![alt](/Introducao/imagem/box_model_4.jpg)

# Unidades de Medida

## Length e Width

Atributos de tamanho e largura, respectivamente; esses atributos irão receber as unidades de medida.

## Medidas Absolutas

- Centrimetro = cm
- Milimetro = mm
- Polegadas = in
- Pixel = px
- Pontos = pt
- Paicas = pc

## Medidas Relativas

- Porcentagem = %
- M [é o tamanho de uma letra M usada como referencia] = em
- Root M [é o tamanho da letra M da pagina] = rem
- Unidade do caracter [é o tamanho do caracter da fonte] = ch
- X [é o tamanho da letra X usado como referencia] = ex
- Largura da Janela de Exibição [é a area vísivel da página] = vw
- Altura da Janela de Exibição [é a area vísivel da página] = vh
- Janela de Exibição Minima [quebra o menor parte (largura ou altura) e divide em 100 partes] = vmin
    1. Numa tela de tamanho 1024x760, vmin usará a medida vh (760), portanto 1vmin = 7.6 (760 / 100)
- Janela de Exibição Maxima [quebra o maior parte (largura ou altura) e divide em 100 partes] = vmax
    1. Numa tela de tamanho 1024x760, vmax usará a medida vh (1024), portanto 1vmin = 10.24 (1024 / 100)
- Fração Restante [divide a tela em frações] = fr

##


